import React, { PropTypes as T } from 'react'
import { connect } from 'react-redux'

// import List, { ListItem } from 'material-ui/List'

import styles from '../../css/githubInfo.css'

export default class Container extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      fetchError: false,
      repoArray:[]
    }
  }

  componentWillMount() {
    // fetch('https://api.github.com/users/Gethyl/repos')
    //   .then(res => res.json())
    //   .then(res => {
        // console.info("Response",res)
        
        // const repoArray = res.map(item => {
        //   return {
        //     repoName: item.name
        //   }
        // })
        const repoArray = [
            {repoName: "AngularJsWithReact"},
            {repoName: "ApolloGraphQLExample"},
            {repoName: "Auth0GraphQLReactExample"}];

        this.setState({ repoArray, fetchError: false })
      // })
      // .catch(err => {
      //   // console.error("Error ", err)
      //   this.setState({ fetchError: true })
      // })
  }

  componentDidUpdate(prevProps, prevState) {
  }

  // componentWillMount() {
  // }

  render() {
    debugger;
    const { repoArray } = this.state;
    return (
      <div>
        <hr />
        <h4 style={{ 'textAlign': 'center' }}>Github Repositories </h4>
        <hr />
        {!!repoArray &&
          <ul>
            <div>
            {
              repoArray.map((repo ,i)=> <li key={ i }> { repo.repoName } </li> )
            }            
            </div>
            </ul>
        }
        {!!this.state.fetchError && <h3>Unexpected Error or Probably Offline.. :|</h3>}
      </div>
    )
  }
}